To run:

Install Node either through package manager or installer

Download or clone project to local directory

Go to project directory in Node command prompt

"npm install" to install dependencies

- "npm start" to start
- "npm test" to run basic test
- "npm run build" to build

-------------

Features:

- Fully semantic HTML5 for accessibility.
- SCSS compiled on build.
- React application built on approved create-react-app scaffolding.
- Modularity is used where appropriate, for simplicity and ease of modification.
- Robust Bootstrap-based mobile-first responsive layout, pixel perfect to the design where practical.
- Code comments where useful.
- Separation of content, functionality and presentation where appropriate.
- Thoroughly tested on a screen reader, Browserstack and actual browsers & devices.

-------------

Possible improvements:

- Minify compiled CSS.
- Font size click-to-adjust and high contrast mode.
- Back-to-top floating button and smooth scroll on anchor.
- Build a basic CMS that writes to content.js.
- More detailed automated tests and a TDD approach.
- Eject from create-react-app scaffolding for more under-the-hood customisation.