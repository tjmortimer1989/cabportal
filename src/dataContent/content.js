/* Page content, ideal for use with a CMS which outputs to these objects */

/* START HEADER ITEMS */

/* Top menu items */
export const headerMenuItems = [
  {
    text: 'Benefits', link: '/benefits'
  }, 
  {
    text: 'Work', link: '/work'
  }, 
  {
    text: 'Debt & Money', link: '/debtmoney'
  }, 
  {
    text: 'Consumer', link: '/consumer'
  }, 
  {
    text: 'Family', link: '/family'
  }, 
];

/* Hamburger (cheeseburgers are better!) menu items */
export const cheeseBurgerItems = [
  'Housing', 
  'Law', 
  'Immigration',
  'Health',
  'More'
];

/* Self explanatory sub header text, displayed just below the Jumbotron */
export const subHeaderText = "Welcome to Citizens Advice. How can we help? Welcome to Citizens Advice. How can we help? Welcome to Citizens Advice.";

/* Customisable text content for our button */
export const subHeaderButton = "More Info";

/* END HEADER ITEMS */

/* START BODY ITEMS */

/* Our main body headline */
export const bodyTitle = "We aim to provide the advice people need for the problems they face.";

/* Object containing every paragraph in our main column. Each entry is a new para, and can have a header attached */
export const bodyText = [
  {
    bodyTextHeading: 'We help people find a way forward. We help people find a way forward...',
    bodyTextContent: 'Citizens Advice provides free, confidential and independent advice to help people overcome their problems. We are a voice for our clients and consumers on the issues that matter to them. Citizens Advice provides free, confidential and independent advice...'
  },
  {
    bodyTextContent: 'Citizens Advice provides free, confidential and independent advice to help people overcome their problems. We are a voice for our clients and consumers on the issues that matter to them. Citizens Advice provides free, confidential and independent advice...'
  },
  {
    bodyTextHeading: 'We’re a part of the local community. We’re a part of the local community. We’re a part of the local community. We’re a part of the local...',
    bodyTextContent: 'We also give advice on consumer rights on our consumer helpline, support witnesses in courts through the Witness Service and give pension guidance to people aged over 50. We also give advice on consumer rights on our consumer helpline...'
  }
] 

/* Helpfully-labelled object containing the text for our quote boxout  */
export const boxoutText = {
  boxoutTitle: 'They helped me a lot. They helped me...',
  boxoutQuote: 'When I visited the CAB office in Derby, the issue was pretty much resolved for me there and then. When I visited the CAB office in Derby, the issue was pretty much resolved for me there and then.',
  boxoutName: '"Christmas Pudding"',
  boxoutLoc: 'Derby, GB',
  boxoutSource: 'Trustpilot'
}

/* END BODY ITEMS */

/* START FOOTER ITEMS */

/* Each item in this object is its own column. The columnList arrays are the items in each column  */
export const footerContent = [
  { columnHeader: 'More',
    columnList: [
      'Jobs', 
      'Media', 
      'Volunteering'
    ]
  },
  { columnHeader: 'Resources',
    columnList: [
      'Accessibility', 
      'A-Z', 
      'Search'
    ]
  }
];

/* If we had a CMS plugged in, our content editor would be free to insert any social link. The icon is automatically picked up from the title - it must be lower case!*/
export const socialContent = [ 
  { socialHeader: 'Follow us',
    socialList: [
      {url:'https://www.facebook.com',title:"facebook"}, 
      {url:'https://www.twitter.com',title:"twitter"},
      {url:'https://www.dribbble.com',title:"dribbble"}
    ]
  }
];

/* END FOOTER ITEMS */