/* Imports inner component templates, the Bootstrap components we're using and title content */
import React from 'react';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import BodyTitleTemplate from '../InnerComponents/BodyTitleTemplate/BodyTitleTemplate'
import BodyTextTemplate from '../InnerComponents/BodyTextTemplate/BodyTextTemplate';
import BodyBoxoutTemplate from '../InnerComponents/BodyBoxoutTemplate/BodyBoxoutTemplate';
import { bodyTitle, boxoutText, bodyText } from '../dataContent/content';

class BodyTemplate extends React.Component {

  render() {

    return (
        /* Using React Bootstrap's native syntax where practical */
        <Container role="main">
          <Row>
            <Col md={{ span: 10, offset: 1 }}>
              <BodyTitleTemplate 
                content={bodyTitle}  
              />
            </Col>
          </Row>
          <Row>
            {/* Semantic HTML with Bootstrap class instead of a React Bootstrap Col is preferred here */}
            <aside className="col-md-4 boxOut">
              {/* Fetch content from boxoutText, with presentational code nested in component */}
              <BodyBoxoutTemplate 
                boxoutTitle={boxoutText.boxoutTitle}
                boxoutQuote={boxoutText.boxoutQuote}
                boxoutName={boxoutText.boxoutName}
                boxoutLoc={boxoutText.boxoutLoc}
                boxoutSource={boxoutText.boxoutSource}
              />
            </aside>
            {/* Semantic is also better here */}
            <article role="group" className="col-md-7 offset-md-1 mainText">
              {/* For each paragraph in bodyText data, fetch heading (if present) & content */}
              {bodyText.map(bodyParagraph => (
                <BodyTextTemplate 
                  bodyTextHeading={bodyParagraph.bodyTextHeading}
                  bodyTextContent={bodyParagraph.bodyTextContent}
                />
              ))}
            </article>
          </Row>
        </Container>
    )
  }
}

export default BodyTemplate;