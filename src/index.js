/*Imports React scaffolding and app*/
import React from 'react';
import ReactDOM from 'react-dom';
import MainApp from './MainApp/MainApp';

/*Renders app to "root" div container*/
ReactDOM.render(<MainApp />, document.getElementById('root'));