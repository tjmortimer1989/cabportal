/* Import React component, styling and inner templates */
import React, { Component } from 'react';
/* We load a nice small, compiled, minified CSS file client-side as the SCSS compile is done on build */
import './MainApp.css';
import HeaderTemplate from '../HeaderTemplate/HeaderTemplate';
import BodyTemplate from '../BodyTemplate/BodyTemplate';
import FooterTemplate from '../FooterTemplate/FooterTemplate';

class MainApp extends Component {

  /*Renders main application and inner templates */
  render() {

    return (

      <div className="mainApp">
        <HeaderTemplate />
        <BodyTemplate />
        <FooterTemplate />
      </div>

    );
  }
}

export default MainApp;