import React from 'react';
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import FooterContent from '../InnerComponents/FooterContent/FooterContent';
import SocialFooterContent from '../InnerComponents/SocialFooterContent/SocialFooterContent';
import {footerContent} from '../dataContent/content';
import {socialContent} from '../dataContent/content';
import logo from '../dataContent/cabLogo.png';

function FooterTemplate(props) {

return (
  <Container>
    {/* Semantics over React syntax here */}
    <footer className="row">
      <Col sm={3}>
        <img src={logo} alt="CAB Logo" />
      </Col>
      {/* The layout code for our footer section is kept within the nested components */}
      {footerContent.map((column, each) => (    
        <FooterContent
          columnHeader={footerContent[each].columnHeader}
          columnList={footerContent[each].columnList}
         />
      ))}
      {socialContent.map((social, each) => (    
        <SocialFooterContent
          socialHeader={socialContent[each].socialHeader}
          socialList={socialContent[each].socialList}
         />
      ))}
    </footer>
  </Container>
  )
}

export default FooterTemplate;