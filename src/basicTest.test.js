/* Required imports for basic test */
import React from 'react';
import ReactDOM from 'react-dom';
import MainApp from './MainApp/MainApp';

/* Basic test of correct rendering */
it('successfully renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MainApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
