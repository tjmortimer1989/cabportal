/* Import React, the necessary Bootstrap components for this template, and the content we need */
import React from 'react';
import JumboTron from '../InnerComponents/JumboTron/JumboTron';
import SubHeader from '../InnerComponents/SubHeader/SubHeader';
import HeaderMenu from '../InnerComponents/HeaderMenu/HeaderMenu';
import CheeseBurgerMenu from '../InnerComponents/CheeseBurgerMenu/CheeseBurgerMenu';
import { subHeaderText, subHeaderButton, headerMenuItems, cheeseBurgerItems } from '../dataContent/content';

class HeaderTemplate extends React.Component {

  render() {

    return (
    /* Very modular, separating our two menus and main content blocks in this component */
    <header>
      <HeaderMenu headerMenuItems={headerMenuItems} />
      <CheeseBurgerMenu
        cheeseBurgerItems={cheeseBurgerItems}
      />
      <JumboTron />
      <SubHeader
        content={subHeaderText}
        buttonlabel={subHeaderButton}
      />
    </header>
    )
  }
}

export default HeaderTemplate;