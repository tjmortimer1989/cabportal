/* We only need Col here from Bootstrap, no need to import the rest */
import React from 'react';
import Col from 'react-bootstrap/Col'

function FooterContent(props) {
const { columnHeader, columnList } = props;
  
return (
  /* Each item in the footerContent array will be 2 col wide */
  <Col sm={2}>
    <p tabIndex="0">
      {columnHeader}
    </p>
    <ul>
      {/* Print each item in a specific column to an LI */}
      {columnList.map(li => (  
        <li>
          <a href={li} title={li} tabIndex="0">{li}</a>
        </li>
      ))}
    </ul>
  </Col>
  )
}

export default FooterContent;