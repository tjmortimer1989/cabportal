import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function SubHeader(props) {
const { content, buttonlabel } = props;

return (
	/* Using the very handy React Bootstrap responsive grid for this segment */
	<div className="subHeader">
		<Container>
		    <Row>
		      <Col sm={9}>
		      	<p tabIndex="0">{content}</p>
		      </Col>
		      <Col sm={3} className="brandButton">
		      	<a className="btn" href="#bodyTitle" title={buttonlabel} tabIndex="0">{buttonlabel}</a>
		      </Col>
		    </Row>
	    </Container>
	</div>
  )
}

export default SubHeader;