/* Nav components from Bootstrap give us clean React syntax AND native CSS animation */
import React from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

function CheeseBurgerMenu(props) {
  const { cheeseBurgerItems } = props;
  
  return (
    /* Standard React Bootstrap navbar component, with added role */
    <Navbar bg="light" expand="never" role="navigation" className="navbar-dark">
      <Navbar.Toggle aria-controls="basic-navbar-nav">
        <span className="navbar-toggler-icon"></span>
        <span>Menu</span>
      </Navbar.Toggle>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {/* Insert a new NavLink for each "burgerOption" in our cheeseBurgerMenu */}
          {cheeseBurgerItems.map(burgerOption => (
            <Nav.Link href={burgerOption} title={burgerOption} aria-labelledby={burgerOption} tabIndex="0">{burgerOption}</Nav.Link>
          ))}
        </Nav>
      </Navbar.Collapse>
    </Navbar>

  )
}

export default CheeseBurgerMenu;