import React from 'react';
import Nav from 'react-bootstrap/Nav';

function HeaderMenu(props) {
  const { headerMenuItems } = props;

  return (
    /* Standard React Bootstrap main nav, pulling through from our content file */
    <Nav
      aria-label="Navigation" role="navigation"
      className ="justify-content-center"
    >
      {headerMenuItems.map(menuOption => (
        <Nav.Item>
          <Nav.Link href={menuOption.link} title={menuOption.text} aria-labelledby={menuOption.text} tabIndex="0">{menuOption.text}</Nav.Link>
        </Nav.Item>
      ))}
    </Nav>
  )
}

export default HeaderMenu;