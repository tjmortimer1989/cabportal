/* Same structure as FooterContent */
import React from 'react';
import Col from 'react-bootstrap/Col'

function SocialFooterContent(props) {
const { socialHeader, socialList } = props;
  
return (
  /* Create social column - there's only one currently, but we can add more in the array */
  <Col sm={2} className="social">
    <p tabIndex="0">
      {socialHeader}
    </p>
    <ul>
      {/* Print each item in our social entries, with correct name and socicon */}
      {socialList.map(social => (  
        <li>
          <a href={social.url} title={social.title} tabIndex="0">
            <i className={"socicon-"+social.title}></i>
          </a>
        </li>
      ))}
    </ul>
  </Col>
  )
}

export default SocialFooterContent;