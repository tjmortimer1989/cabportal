/* Import the Bootstrap Jumbotron, its Container dependency and the logo */
import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import logo from '../../dataContent/cabLogo.png';

function JumboTron() {
  return (
  /* Behold the JUMBOTRON! (with semantically correct h1 and tabIndex for accessibility) */
   <Jumbotron fluid>
	  <Container>
	  	{/* Advice is hardcoded here, as it is the company name or tagline and unlikely to change regularly */}
	  	<h1 tabIndex="0"><img src={logo} alt="Citizens Advice Logo" />Advice</h1>
	  </Container>
	</Jumbotron>
  )
}

export default JumboTron;