import React from 'react';

function BodyTitleTemplate(props) {
  const { content } = props;
  /*Renders main h2 for body with tabIndex for key focus in accessibility modes*/
  return (
  	/* Body header is available to change in our data, for different campaigns etc */
    <h2 className="bodyTitleContent" id="bodyTitle" tabIndex="0">{content}</h2>
    )
  }

export default BodyTitleTemplate;