import React from 'react';

function BodyTextTemplate(props) {
  const { bodyTextHeading, bodyTextContent } = props;

  /* If paragraph doesn't have a heading, only render bodyTextContent from data */
  if (!bodyTextHeading) {
    return (
	    <React.Fragment>
	      <p tabIndex="0">{bodyTextContent}</p>
	    </React.Fragment>
    )
  }

  /* If paragraph has a heading, render both bodyTextHeader and bodyTextContent from data */
  else {
    return (
	    <React.Fragment>
	      <h4 tabIndex="0">{bodyTextHeading}</h4>
	      <p tabIndex="0">{bodyTextContent}</p>
	    </React.Fragment>
    )
  }
    
}

export default BodyTextTemplate;