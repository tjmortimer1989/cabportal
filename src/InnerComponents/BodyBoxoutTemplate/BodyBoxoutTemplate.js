import React from 'react';

function BodyBoxoutTemplate(props) {
  const {boxoutTitle, boxoutQuote, boxoutName, boxoutLoc, boxoutSource} = props;
  
  return (
    /*Semantically correct quote boxout fragment */
    <React.Fragment>
      <h5 tabIndex="0">{boxoutTitle}</h5>
      <blockquote tabIndex="0">{boxoutQuote}</blockquote>
      <div rel="author">
        <p tabIndex="0">{boxoutName}</p>
        <p tabIndex="0">{boxoutLoc}</p>
        <p tabIndex="0">{boxoutSource}</p>
      </div>
    </React.Fragment>
  )
}

export default BodyBoxoutTemplate;